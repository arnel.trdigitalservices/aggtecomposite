<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>All rights reserved</b>
    </div>
     <strong>Copyright &copy; <?php echo date("Y") ?> <a href="https://www.aggtedeck.com">AGGTEDeck WPC Cladding and Decking</a></strong>
</footer>