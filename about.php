<?php if (isset($_SESSION['C_ID']))?>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<title>About AGGTEDeck Composite Cladding & Decking</title>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="description" content="AGGTEDeck is specialists in composite decking – it’s all we do! We’ve also made sure that we can provide the service that backs it up, offering convenient, fast delivery, and superior customer support.">
<meta name="keywords" content="About the company, AGGTE ,WPC, Composites, AGG, GoGreen, Quality Cladding, Quality Decking, Gates, Wood Plastic Composite, Specialists in composite decking, ">
<meta name="author" content="Janzen Go">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Pinterest Metatag -->
 <meta name="p:domain_verify" content="6f587306ee8b4c60fb96f108aac158ac"/>
<!-- Pinterest Metatag End -->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '198102358516938');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=198102358516938&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K4S5CH8');</script>
<!-- End Google Tag Manager -->


<!-- Fav Icon -->
<link class="logoicon" rel="shortcut icon" href="favicon.ico">
<!-- Canonical -->

<link rel=“canonical” href=“https://www.aggtedeck.com/about” />

<!-- Local Schema -->
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "LocalBusiness",
  "name": "AGGTEDeck Composite Cladding & Decking",
  "image": "https://www.aggtedeck.com/images/logo3.jpg",
  "@id": "https://www.aggtedeck.com/",
  "url": "https://www.aggtedeck.com/",
  "telephone": "Globe 0916 377 0871  /  Smart 0929 3185482",
  "priceRange": "Php 5.00 - Php 920.00",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "Bagong Daan, Diversion Road, Barangay Cawit",
    "addressLocality": "Taal, Batangas",
    "postalCode": "4208",
    "addressCountry": "PH"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": 13.897579111806225,
    "longitude": 120.92305571076798
  },
  "openingHoursSpecification": {
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday"
    ],
    "opens": "08:00",
    "closes": "17:00"
  },
  "sameAs": [
    "https://www.facebook.com/aggtedeck",
    "https://twitter.com/AggteD",
    "http://instagram.com/aggtedeck",
    "https://www.linkedin.com/company/aggtedeck",
    "https://www.pinterest.ph/aggtedeck",
    "https://www.aggtedeck.com"
  ]
}
</script>
<?php include 'links.php'; ?>
</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K4S5CH8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <!-- Load Facebook SDK for JavaScript -->
<?php include 'fbplug.php'; ?>
<!--Load Facebook SDK for JavaScript End Here-->
<div class="page-wrapper">
  <!--preloader start-->
<div id="preloader" class="preloader">
</div>
<script
        src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="
        crossorigin="anonymous"></script>
<script>
    $(function() {
        // Cookies
        function setCookie(name, value, days) {
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var expires = "; expires=" + date.toGMTString();
            }
            else var expires = "";

            document.cookie = name + "=" + value + expires + "; path=/";
        }

        function getCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }

//        Validate cookie
        var myCookie = getCookie("MyCookie");
        if (myCookie == null) {
//                alert('No cookei');
            $('.preloader').css('display','block');
            setCookie("MyCookie", "foo", 7);
        }
        else {
//                alert('yes cookei');
            $('.preloader').css('display','none');
        }
    });
</script>
  <!--preloader end-->
    <!--main-header start-->
        <?php include 'header.php'; ?>
    <!--main-header-end-->
  <section class="inner-heading">
    <div class="container">
      <h1>About US</h1>
      <ul class="xs-breadcumb">
        <li><a href="index.php"> Home  / </a> <a href="index.php">Pages / </a>About US</li>
      </ul>
    </div>
  </section>

  <!--about-info start-->
  <section class="about-info">
    <!--container start-->
    <div class="container">
      <!--row start-->
      <div class="row">
        <!--col start-->
        <div class="col-md-6 col-sm-push-6">
          <div class="section-title">
            <h3> <span>About our Company</span></h3>
            <h4>DECKING AND CLADDING NEEDS? COME TO US FOR QUALITY YOU CAN TRUST!</h4>
              <div class="section-par">
              <p>It was in the year 2016 when AGG Trading Enterprise ventured into the composite decking and cladding industry, thus becoming one of the Philippines’ trusted distributors of wood-alternative <a target="_blank" href="https://blog.advantagelumber.com/category/composite-decking/">decking and cladding materials</a>. From its humble beginnings as a composite decking supplier, AGGTE has always believed that superior product quality does not necessarily need to come from high cost. From this principle which had served as the company’s mantra and guiding light, the business continues to flourish over the years and has earned its reputation as a leading supplier of quality decking and cladding materials among different local-based builder merchants in the area.</p><br>

            <p> In its five years of existence with thousands of boards produced, AGGTE is still firm to continue with its commitment to that none of its competitor has surpassed: to provide products of premium quality at a very reasonable and affordable price.</p>
                </div>
          </div>
          <div class="aboutUs-description">
            <div class="row">
              <!--col start-->
              <div class="col-sm-4 counter-item">
                <div class="single-about"><i class="fa fa-list-ul" aria-hidden="true"></i> <span class="counter_number" data-from="1" data-to="<?php echo $years?>" data-speed="1200"><?php echo $years?></span>
                  <p>year of experience</p>
                </div>
              </div>
              <!--col end-->

              <!--col start-->
              <div class="col-sm-4 counter-item">
                <div class="single-about"> <i class="fa fa-thumbs-up" aria-hidden="true"></i> <span class="counter_number" data-from="1" data-to="84" data-speed="3000">84</span>
                  <p>Happy Customer</p>
                </div>
              </div>
              <!--col end-->
              <!--col start-->
              <div class="col-sm-4 counter-item">
                <div class="single-about"> <i class="fa fa-trophy" aria-hidden="true"></i> <span class="counter_number" data-from="1" data-to="<?php echo $years?>" data-speed="5000">5</span>
                  <p>Professional Awards</p>
                </div>
              </div>
              <!--col end-->
              <!--col start-->
            </div>
          </div>
        </div>
        <!--col end-->
        <!--col start-->
        <div class="col-md-6 col-sm-pull-6">
          <div class="about-img"><br><br><br>
            <figure> <img src="images/about.jpg" class="img-responsive" alt="Image"></figure>
          </div>
            <div name="divHrefB" style="height: 0px;width: 0px;overflow:hidden;">
                <a href='https://www.freepik.com/vectors/sale'>Sale vector created by macrovector - www.freepik.com</a>
            </div>
        </div>
        <!--col start-->
      </div>
      <!--row end-->
    </div>
    <!--container end-->
  </section>
  <!--about-info end-->
<!-- INSERT WHY CHOOSE US HERE! -->
  <section class="aboutWrap">
      <div class="container" style="margin-bottom: 30px;">
      <div class="section-title whyChooseSecTitle">
        <h3>Why <span>Choose </span>Us?</h3>
      </div>
        <!--row start-->
      <div class="row serviceList">
        <!--col start-->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="contact-item">
            <div class="fig_caption">
              <div class="icon"><i class="fa fa-users" aria-hidden="true"></i> </div>
              <div class="details">
                <h3>Workers</h3>
                <p>Our workers are all well-versed about our products because they have undergone advanced training and were privileged to join a pool of professionals with very extensive <a target="_blank" href="https://en.wikipedia.org/wiki/Wood-plastic_composite">WPC Cladding and Decking</a> experience in the industry.</p><br> <br> <br> <br>
              </div>
            </div>
          </div>
        </div>
        <!--col end-->
        <!--col start-->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="contact-item">
            <div class="fig_caption">
              <div class="icon"><i class="fa fa-wrench" aria-hidden="true"></i> </div>
              <div class="details">
                <h3>Repairs</h3>
                <p>Our company assures quick and immediate response upon <a href="contact.php#contact-section">inquiries and request</a> addressed to us. To ensure excellent delivery of service, well-trained subcontractor builders are being assigned by the management. The team shall attend to the concerns of the clients and take action within 3 to 5 working days after receiving the information from the client.</p> 
              </div>
            </div>
          </div>
        </div>
        <!--col end-->
        <!--col start-->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="contact-item">
            <div class="fig_caption">
              <div class="icon"><i class="fa fa-th-large" aria-hidden="true"></i> </div>
              <div class="details">
                <h3>Materials</h3>
                <p>With a blend of elegance and strength, or come together as one weather-shrugging, long-lasting, easy-to-care on indoor or indoor applications, every AGGTE® WPC CLADDING & DECKING piece can stand on its own and this is its advantage over its competitors.</p> <br> <br> <br>
              </div>
            </div>
          </div>
        </div>
        <!--col end-->
        <!--col start-->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="contact-item">
            <div class="fig_caption">
              <div class="icon"><i class="fa fa-truck" aria-hidden="true"></i> </div>
              <div class="details">
                <h3>Delivery</h3>
                <p>We are determined to dispatch all your orders within one-working day. However, we advise you to give us at least 3-5 working days to deliver your goods after you have confirmed your orders. As an alternative, items may be collected at our warehouse in Taal, Batangas upon arrangement. Please call us on<a target="_blank" href="contact.php"> 09163770871 </a>to discuss this service if you prefer it.</p>
              </div>
            </div>
          </div>
        </div>
        <!--col end-->
      </div>
      <!--row end-->
  </div>
    <section>
  <!--about-info end-->
  <section id="advantage" class="why_choose_us_sec" style="background: #fff">
    <div class="container">
      <div class="section-title">
        <h3>THE <span>ADVANTAGE</span></h3>
      </div>
      <div class="row advan">
          <h4 style="text-align: center">THE HASSLE-FREE ALTERNATIVE TO WOOD DECKING</h4>
          <br>
          <p>WPC deck is really hassle-free, in fact, it is ready whenever you want to use it. You may say goodbye to sanding, staining and painting because with our WPC deck all you need is a little soap and water or a quick power washing for its maintenance requirement. Compared to wood which requires constant care, <a target="_blank" href="http://www.cxhanming.com/what-is-the-wpc-decking-meaning/#:~:text=WPC%20is%20made%20of%20a,to%20other%20types%20of%20decking.&text=It%20boasts%20low%20maintenance%20and%20a%20much%20longer%20lifespan%20than%20wood%20decking.">AGGTE composite decking</a> is more durable, long lasting and would require less maintenance. Looking at everything that AGGTE has to offer, wood decking begins to feel like signing up for your second job.</p>
      </div>
        <br>
        <h4 style="text-align: center;font-weight: 700;color: #555">The hassles that come with wood</h4>
        <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="single-item center-align" style="border: solid 1px white; padding:40px 5px">
              <img class="advantage-image" src="images/painting.jpg">
              <p style="padding-top: 10px">Needs seasonal painting, staining or sealing</p>
        </div>
      </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="single-item center-align" style="border: solid 1px white; padding:40px 5px">
              <img class="advantage-image" src="images/rotting.jpg">
              <p style="padding-top: 10px">Becomes a safety hazard when it rots, splits and splinters</p>
        </div>
      </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="single-item center-align" style="border: solid 1px white; padding:40px 5px">
              <img class="advantage-image" src="images/stain.jpg">
              <p style="padding-top: 10px">Fades and stains easily, showing every spill and scuff</p>
        </div>
      </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="single-item center-align" style="border: solid 1px white; padding:0px 1px" >
              <img class="advantage-image" src="images/termites.jpg">
              <p style="padding-top: 10px">Insects can cause extensive structural damage</p>
        </div>
      </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="single-item center-align" style="border: solid 1px white; padding:0px 1px" >
              <img class="advantage-image" src="images/deforestation.jpg">
              <p style="padding-top: 10px">Contributes to deforestation, cutting down trees that local wildlife depend on for food and shelter </p>
        </div>
      </div>
        <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="single-item center-align" style="border: solid 1px white; padding:0px 1px" >
              <img class="advantage-image" src="images/burning.jpg">
              <p style="padding-top: 10px">Because of it being easily burnt, wood can be very dangerous when near a place where there is always fire like the kitchen.
 </p>
        </div>
      </div>
        <a data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target=".bs-example-modal-md-2"><img class="advantage-image-main" src="images/wood.jpg"></a>
    </div>
  </section>
  <!--why_choose_us_sec end-->
    <!--footer-sec-start-->
      <?php include 'footer.php'; ?>
    <!--footer-sec-end-->
</div>
<!--scroll-to-top start-->
<!--<div class="scroll-to-top scroll-to-target" data-target="html"><span class="icon fa fa-angle-up"></span></div>-->
<!--scroll-to-top end-->
<script type = "text/javascript">
            function comfirmFunc() {
               window.open("https://shopee.ph/Wood-Plastic-Composite-(WPC)-SAMPLE-Decking-or-Cladding-i.74758878.4843042912", '_blank');
            }

      </script>
</body>

</html>
