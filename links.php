<html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link href="css/upgrade.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.1/css/font-awesome.min.css" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="dist/color-default.css"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" integrity="sha512-+EoPw+Fiwh6eSeRK7zwIKG2MA8i3rV/DGa3tdttQGgWyatG/SkncT53KHQaS5Jh9MNOT3dmFL0FjTY08And/Cw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link href="css/animate.css" rel="stylesheet">
<link href="css/owl.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" integrity="sha512-FKK36sqWcrWs0CJeDVz3UhCFH5XnRAU9fijKZmn2760FDeYmBB7oos2E4V2vS9KjuKDy5rXZa6L1gIjnsDJ9bQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link href="css/style_slider.css" rel="stylesheet">
<link href="css/modal-designs.css" rel="stylesheet">
<link href="css/custom.css" rel="stylesheet">
<link href="css/altModals.css" rel="stylesheet">
</html>