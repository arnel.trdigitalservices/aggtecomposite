 <!--portfolio-area start-->
 <section class="bg-gray portfolio-area">
   <!--container start-->
   <div class="container-fluid">
     <div class="section-title">
       <h3>OUR RECENT <span>PORTFOLIOS</span></h3>
       <p>
       <h4>Here's our latest and most commendable projects all throughout the years 2019-<?php echo date("Y") ?>.</h4>
       </p>
     </div>
     <!--row start-->
     <div class="row">
       <ul class="container-filter categories-filter">
         <li> <a class="categories hvr-link" data-filter="*" id="all1" onclick="Focus1()" style="background-color: darkgreen;color:white">All Projects</a> </li>
         <li> <a class="categories hvr-link" data-filter=".cladding" id="all2" onclick="Focus2()">Cladding</a> </li>
         <li> <a class="categories hvr-link" data-filter=".decking" id="all3" onclick="Focus3()">Decking</a> </li>
         <li> <a class="categories hvr-link" data-filter=".gates" id="all4" onclick="Focus4()">Gates</a> </li>
       </ul>
     </div>
     <!--row end-->
     <div class="portfolio-inner">
       <!--row start-->
       <ul class="row container-masonry  portfolio-posts grid">
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item grid-sizer gates">
           <div class="image-hover-effect-4"> <img src="images/gallery/gates/1.webp" alt="img-1">
             <div class="caption">
               <h3>WPC Gate</h3>
               <p>Quezon City, Metro Manila</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/gates/1.webp" title="WPC Gates" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item grid-sizer gates">
           <div class="image-hover-effect-4"> <img src="images/gallery/gates/2.webp" alt="img-3">
             <div class="caption">
               <h3>WPC Gate</h3>
               <p>Imus, Cavite</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/gates/2.webp" title="WPC Gate" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item branding cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/1.webp" alt="img-4">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Batangas City</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/1.webp" title="WPC Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/2.webp" alt="img-5">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Laguna</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/2.webp" title="WPC Decking Lanai" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/2.webp" alt="img-7">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Orani, Bataan</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/2.webp" title="WPC Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/1.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Taal, Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/1.webp" title="WPC Decking Lanai" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/3.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Quezon City, Metro Manila</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/3.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/7.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Parañaque, Metro Manila</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/7.webp" title="WPC Decking Stairway" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item gates">
           <div class="image-hover-effect-4"> <img src="images/gallery/gates/3.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Gates</h3>
               <p>Isabela</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/gates/3.webp" title="WPC Accent Gate" class="lightbox-image"><i class="fa fa-search"></i></a></div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/17.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Cainta, Rizal</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/17.webp" title="WPC Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a></div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/18.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Calamba, Laguna</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/18.webp" title="WPC Decking Poolside" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/3.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Nasugbu, Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/3.webp" title="WPC Decking Poolside" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item gates">
           <div class="image-hover-effect-4"> <img src="images/gallery/gates/4.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Gates</h3>
               <p>Las Piñas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/gates/4.webp" title="WPC Accent Gate" class="lightbox-image"><i class="fa fa-search"></i></a></div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/12.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Nayomi Resort, Balete Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/12.webp" title="WPC Decking Poolside" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/4.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Batangas City</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/4.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/11.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Quezon City </p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/11.webp" title="WPC Decking Lanai" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/4.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Nueva Ecija</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/4.webp" title="WPC Decking Poolside" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/13.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Kawayan Cove, Nasugbu Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/13.webp" title="WPC Decking Lanai" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item gates">
           <div class="image-hover-effect-4"> <img src="images/gallery/gates/14.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Gates</h3>
               <p>Olongapo City</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/gates/14.webp" title="WPC Accent Gate" class="lightbox-image"><i class="fa fa-search"></i></a></div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/8.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Nasugbu, Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/8.webp" title="WPC Decking Lanai" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/18.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Batangas City</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/18.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/9.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Nasugbu, Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/9.webp" title="WPC Decking Lanai" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/5.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Nueva Ecija</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/5.webp" title="WPC Decking Lanai" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item gates">
           <div class="image-hover-effect-4"> <img src="images/gallery/gates/6.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Taal, Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/gates/6.webp" title="WPC Gates" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/14.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Laguna</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/14.webp" title="WPC Decking Stairway" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item gates">
           <div class="image-hover-effect-4"> <img src="images/gallery/gates/5.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Gates</h3>
               <p>Emilio Aguinaldo, Cavite</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/gates/5.webp" title="WPC Accent Gate" class="lightbox-image"><i class="fa fa-search"></i></a></div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/15.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Parañaque</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/15.webp" title="WPC Decking Stairway" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/5.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Imus, Cavite</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/5.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/10.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Nasugbu, Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/10.webp" title="Accent WPC Decking" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/6.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Orani, Bataan</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/6.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/19.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Sto. Tomas, Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/19.webp" title="Accent Stairway Decking" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/7.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>San Fernando, Pampanga</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/7.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/8.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Camella, Silang Cavite </p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/8.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/20.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Balete, Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/20.webp" title="Accent Decking" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/9.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>San Pablo, Laguna</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/9.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/10.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>San Nicolas, Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/10.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/11.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Santa Rosa, Laguna</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/11.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/12.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Maragondon, Cavite</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/12.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/19.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Bacolod City</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/19.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/20.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Lucena City</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/20.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/13.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Ilocos Sur</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/13.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/21.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>GMA, Cavite</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/21.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/22.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Candelaria Quezon</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/22.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/16.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Ilocos Sur</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/16.webp" title="WPC Decking" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/23.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Mindoro</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/23.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/6.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Pangasinan</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/6.webp" title="WPC Decking" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/14.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Batangas City</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/14.webp" title="Accent Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/21.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Camp Aurora Lumban Laguna</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/21.webp" title="WPC Decking" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/24.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Lucena, Quezon</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/24.webp" title="WPC Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/22.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Las Piñas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/22.webp" title="WPC Decking" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/15.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Bauan, Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/15.webp" title="WPC Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/16.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Sta. Cruz, Laguna</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/16.webp" title="WPC Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/25.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Sta. Cruz, Laguna</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/25.webp" title="WPC Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/26.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Balayan, Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/26.webp" title="WPC Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item gates">
           <div class="image-hover-effect-4"> <img src="images/gallery/gates/7.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Gates</h3>
               <p>Balayan, Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/gates/7.webp" title="WPC Gate" class="lightbox-image"><i class="fa fa-search"></i></a></div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item gates">
           <div class="image-hover-effect-4"> <img src="images/gallery/gates/8.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Gates</h3>
               <p>Real, Quezon</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/gates/8.webp" title="WPC Gate" class="lightbox-image"><i class="fa fa-search"></i></a></div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/27.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>San Juan, Metro Manila</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/27.webp" title="WPC Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item decking">
           <div class="image-hover-effect-4"> <img src="images/gallery/decking/17.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Decking</h3>
               <p>Cavite</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/decking/17.webp" title="WPC Decking" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/28.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Cavite</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/28.webp" title="WPC Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item gates">
           <div class="image-hover-effect-4"> <img src="images/gallery/gates/9.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Gates</h3>
               <p>Real, Quezon</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/gates/9.webp" title="WPC Gate" class="lightbox-image"><i class="fa fa-search"></i></a></div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/29.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Calaca, Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/29.webp" title="WPC Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/30.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Dipolog City, Zamboanga del Norte</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/30.webp" title="WPC Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/31.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Dolores, Quezon</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/31.webp" title="WPC Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/32.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Buli, Taal Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/32.webp" title="WPC Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item cladding">
           <div class="image-hover-effect-4"> <img src="images/gallery/cladding/33.webp" alt="img-8">
             <div class="caption">
               <h3>WPC Cladding</h3>
               <p>Makilala, Cotabato</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/cladding/33.webp" title="WPC Cladding" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item grid-sizer gates">
           <div class="image-hover-effect-4"> <img src="images/gallery/gates/10.webp" alt="img-1">
             <div class="caption">
               <h3>WPC Gate</h3>
               <p>Muzon, Batangas</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/gates/10.webp" title="WPC Gates" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item grid-sizer gates">
           <div class="image-hover-effect-4"> <img src="images/gallery/gates/11.webp" alt="img-1">
             <div class="caption">
               <h3>WPC Gate</h3>
               <p>Tagaytay, Nasugbu</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/gates/11.webp" title="WPC Gates" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item grid-sizer gates">
           <div class="image-hover-effect-4"> <img src="images/gallery/gates/12.webp" alt="img-1">
             <div class="caption">
               <h3>WPC Gate</h3>
               <p>Batasan, Quezon City</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/gates/12.webp" title="WPC Gates" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
         <!--col start-->
         <li class="col-md-3 col-sm-6 col-xs-12 nf-item grid-sizer gates">
           <div class="image-hover-effect-4"> <img src="images/gallery/gates/13.webp" alt="img-1">
             <div class="caption">
               <h3>WPC Gate</h3>
               <p>Santiago, Isabela</p>
             </div>
             <div class="link-wrap"> <a href="images/gallery/gates/13.webp" title="WPC Gates" class="lightbox-image"><i class="fa fa-search"></i></a> </div>
           </div>
         </li>
         <!--col end-->
       </ul>
       <!--row end-->
     </div>
     <!--container end-->
 </section>
 <!--portfolio-area end-->