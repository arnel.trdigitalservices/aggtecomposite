<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<title>AGG WPC Cladding & Decking</title>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="description" content="AGG WPC Cladding Decking">
<meta name="keywords" content="WPC, Composites, AGG, GoGreen, Cladding, Decking, Gates">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Fav Icon -->
<link class="logoicon" rel="shortcut icon" href="../favicon.ico">
<!-- Style CSS -->
<?php include 'links.php'; ?>
</head>
<body>
  <!--preloader end-->

  <!--inner content start-->

  <section class="inner-wrap">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="error-page">
              <br><br><br>
              <h3>Congratulations!</h3>
              <h2 style="font-size: 50px;">Your message was successfully sent!</h2><br>
            <p style="font-size: 20px;">You will be redirected to the main page in few seconds.</p>
              <br><br><br>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 col-sm-12">
          <div class="error-link">
          </div>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
      <br><br>
  </section>

  <!--inner content end-->
<!--footer-sec start-->
<footer class="footer-sec">
  <!--container start-->
  <div class="container">
    <!--row start-->
    <div class="row">
      <!--col start-->
      <div class="col-md-4 col-sm-12">
        <div class="footer-info">
          <div class="footer-logo"> <a href="index.php"><img class="footer-logo-default" src="../images/footer-logo-default.png" alt="AGGTE Default Logo"> </a> </div>
          <p>Never miss out our latest updates online! Follow us on social media to be on top of the news:</p>
          <ul class="footer-social">
            <li><a href="https://www.facebook.com/aquaponics.trading.enterprise/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
            <li><a href="https://twitter.com/share?url=https%3A%2F%2Faquaponicsgogreen.com&text=AquaponicsGoGreen" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
          </ul>
            <br>
            <div class="footer-logo">
                <a href="#">
                    <img class="footer-logo-default" src="../images/other-logos/CE.png" alt="CE Logo">&nbsp;&nbsp;
                    <img class="footer-logo-default" src="../images/other-logos/FSCyc0Nbl.png" alt="FSC Logo">
                    <img class="footer-logo-default" src="../images/other-logos/Greenpeace_symbols_recycle_sign_05s_300x300.png" alt="Recycle Sign">&nbsp;
                    <img class="footer-logo-default" src="../images/other-logos/RoHs.png" alt="RoHS Logo">
                </a>
            </div>
        </div>
      </div>
      <!--col end-->
      <!--col start-->
      <div class="col-md-8">
        <!--row start-->
        <div class="row">
          <!--col start-->
          <div class="col-md-3 col-sm-6">
            <div class="footer-info">
              <h3 class="footer-title">Usefull Links</h3>
              <ul class="service-link">
                <li> <a href="index.php">Home</a> </li>
                <li> <a href="about.php">About Us</a> </li>
                <li> <a href="service-single.html">Services</a> </li>
                <li> <a href="projects-three.html">Gallery</a> </li>
                <li> <a href="faq.html">Faq</a> </li>
              </ul>
            </div>
          </div>
          <!--col end-->
          <!--col start-->
          <div class="col-md-4 col-sm-6">
            <div class="footer-info">
              <h3 class="footer-title">Our Services</h3>
              <ul class="service-link">
                <li> <a href="service-single.html#section-handler1" >WPC Cladding</a> </li>
                <li> <a href="service-single.html#section-handler2">WPC Decking</a> </li>
                <li> <a href="service-single.html#section-handler3">WPC Gates</a> </li>
                <li> <a href="service-single.html#section-handler4">WPC Cladding & Decking Repairs</a> </li>
              </ul>
            </div>
          </div>
          <!--col end-->
          <!--col start-->
          <div class="col-md-5 col-sm-6">
            <div class="footer-info">
              <h3 class="footer-title">Contact Us</h3>
              <ul class="footer-adress">
                <li><i class="fa fa-map-marker"></i><span>Bagong Daan, Diversion Road, Brgy. Cawit, 4208 Taal, Batangas, Philippines</span></li>
                <li><i class="fa fa-phone"></i><span>Globe 0916 377 0871</span></li>
                  <li><i class="fa fa-phone"></i><span>Smart 0929 3185482</span></li>
                  <li><i class="fa fa-phone"></i><span>Landline(043) 740 3512</span></li>
                <li><i class="fa fa-envelope-o"></i><span>sales@aquaponicsgogreen.com</span></li>
              </ul>
            </div>
          </div>
          <!--col end-->
        </div>
        <!--row end-->

      </div>
      <!--col end-->

    </div>
    <!--row end-->
    <div class="copyright-content">
      <!--row start-->
      <div class="row">
        <!--col start-->
       <div class="col-md-6 col-sm-6">
           <p><a target="_blank" href="https://www.facebook.com/wpc.claddingdecking">© 2020 Aquaponics GoGreen All Rights Reserved</a> Design by <a href="#">GoTech Web Solutions</a></p>
          </div>
        <!--col end-->
        <!--col start-->
        <div class="col-md-6 col-sm-6">
          <ul class="footer-bottom-menu">
            <!--<li> <a href="#">Sitemap</a> </li>-->
            <li> <a data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target=".bs-example-modal-md-4">Product Warranty </a> </li>
            <li> <a data-dismiss="modal" aria-label="Close" data-toggle="modal" data-target=".bs-example-modal-md-5">Privacy Policy</a> </li>
          </ul>
        </div>
        <!--col end-->
      </div>
      <!--row end-->
    </div>
  </div>
  <!--container end-->
</footer>
<!--footer-secn end-->
<!--privacy-modal start-->
</div>
</div>
<script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
<?php 
  $origin = $_GET['from'];
  if($origin == 'faq') {
    echo '
        <script>
        $(document).ready(function(){
      setTimeout(function(){
            location.href = "faq.php";
      },4000);
    });
    </script>
    ';
  }
  else if($origin == 'contacts') {
          echo '
          <script>
          $(document).ready(function(){
        setTimeout(function(){
              location.href = "contact.php";
        },4000);
      });
      </script>
      ';
  }
  else if($origin == 'products') {
    echo '
    <script>
    $(document).ready(function(){
  setTimeout(function(){
        location.href = "products/index.php";
  },4000);
});
</script>
';
}
?>

</body>
</html>
